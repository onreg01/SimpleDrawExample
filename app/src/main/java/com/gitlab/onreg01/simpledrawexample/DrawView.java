package com.gitlab.onreg01.simpledrawexample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View {

    private final static String LOG = "CUSTOM_LOG";

    private Paint paint;
    private Path path = new Path();

    public DrawView(Context context) {
        this(context, null);
    }

    public DrawView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Log.d(LOG, "init");
        setOnTouchListener(onTouchListener);
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(15);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(LOG, "onDraw");
        canvas.drawPath(path, paint);
    }

    private OnTouchListener onTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.d(LOG, "ACTION_DOWN");
                    path.moveTo(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.d(LOG, "ACTION_MOVE");
                    path.lineTo(x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    Log.d(LOG, "ACTION_UP");
                    path.reset();
                    break;
            }
            invalidate();
            return true;
        }
    };
}
